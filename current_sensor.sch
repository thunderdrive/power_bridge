EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:power_bridge-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "Monster MK II"
Date "2016-11-02"
Rev "2"
Comp "ThunderDrive"
Comment1 "Current measurement"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3650 2150 4700 2150
Wire Wire Line
	5000 2150 7800 2150
Text HLabel 7800 2150 2    60   Output ~ 0
PHASE_OUT
$Comp
L AMC1304M05DW U2
U 1 1 581A511F
P 6300 3850
F 0 "U2" H 6400 4600 60  0000 C CNN
F 1 "AMC1304M05DW" H 6500 2900 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-16W_7.5x10.3mm_Pitch1.27mm" H 6300 3850 60  0001 C CNN
F 3 "" H 6300 3850 60  0000 C CNN
	1    6300 3850
	1    0    0    -1  
$EndComp
NoConn ~ 5550 3250
NoConn ~ 5550 4050
NoConn ~ 7050 4050
NoConn ~ 7050 4450
NoConn ~ 7050 3450
Wire Wire Line
	5550 3850 4500 3850
Wire Wire Line
	4500 2150 4500 5450
Connection ~ 4500 2150
Connection ~ 4500 3850
Wire Wire Line
	5100 3450 5550 3450
Wire Wire Line
	5100 2150 5100 3450
Wire Wire Line
	4600 3650 5550 3650
Wire Wire Line
	4600 2150 4600 3650
Wire Wire Line
	4300 4250 5550 4250
$Comp
L C C8
U 1 1 581A513E
P 4900 4950
F 0 "C8" V 4850 4550 50  0000 L CNN
F 1 "100n25V" V 4950 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4938 4800 50  0001 C CNN
F 3 "" H 4900 4950 50  0000 C CNN
	1    4900 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5450 5450 5450
Wire Wire Line
	5450 5450 5450 4650
Wire Wire Line
	5450 4650 5550 4650
Connection ~ 4900 5450
$Comp
L C C9
U 1 1 581A5150
P 5150 4950
F 0 "C9" V 5100 4550 50  0000 L CNN
F 1 "100n25V" V 5200 4550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5188 4800 50  0001 C CNN
F 3 "" H 5150 4950 50  0000 C CNN
	1    5150 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5100 5150 5450
Connection ~ 5150 5450
Wire Wire Line
	5550 4450 5150 4450
Wire Wire Line
	5150 4450 5150 4800
Wire Wire Line
	4900 4150 4900 4800
Connection ~ 4900 4250
$Comp
L C C6
U 1 1 581A515D
P 7550 4050
F 0 "C6" H 7600 4150 50  0000 L CNN
F 1 "100n" H 7550 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7588 3900 50  0001 C CNN
F 3 "" H 7550 4050 50  0000 C CNN
	1    7550 4050
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 581A5164
P 7800 4050
F 0 "C7" H 7850 4150 50  0000 L CNN
F 1 "2µ2" H 7800 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 7838 3900 50  0001 C CNN
F 3 "" H 7800 4050 50  0000 C CNN
	1    7800 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4250 8050 4250
Wire Wire Line
	7050 3850 8050 3850
Wire Wire Line
	7050 4650 8050 4650
Wire Wire Line
	7150 4650 7150 3250
Connection ~ 7550 3650
Wire Wire Line
	7150 3250 7050 3250
Connection ~ 7150 4650
Text HLabel 4300 4250 0    60   Input ~ 0
GATE_DRIVE_SUPPLY
Text HLabel 3650 2150 0    60   Input ~ 0
PHASE_IN
Text HLabel 8050 4250 2    60   Output ~ 0
DATA
Text HLabel 8050 3850 2    60   Input ~ 0
CLOCK
$Comp
L PWR_FLAG #FLG08
U 1 1 5821A6C0
P 4900 4150
F 0 "#FLG08" H 4900 4245 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 4330 50  0000 C CNN
F 2 "" H 4900 4150 50  0000 C CNN
F 3 "" H 4900 4150 50  0000 C CNN
	1    4900 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4650 7800 4200
Wire Wire Line
	4900 5100 4900 5650
$Comp
L PWR_FLAG #FLG09
U 1 1 58268774
P 4900 5650
F 0 "#FLG09" H 4900 5745 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 5830 50  0000 C CNN
F 2 "" H 4900 5650 50  0000 C CNN
F 3 "" H 4900 5650 50  0000 C CNN
	1    4900 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7800 3650 7800 3900
Wire Wire Line
	7050 3650 8050 3650
$Comp
L R R4
U 1 1 5831806E
P 4850 2150
F 0 "R4" V 4930 2150 50  0000 C CNN
F 1 "0mR2" V 4850 2150 50  0000 C CNN
F 2 "power_bridge:WSL5931-2Pol" V 4780 2150 50  0001 C CNN
F 3 "" H 4850 2150 50  0000 C CNN
	1    4850 2150
	0    1    1    0   
$EndComp
Connection ~ 4600 2150
Connection ~ 5100 2150
Wire Wire Line
	7550 4200 7550 4650
Connection ~ 7550 4650
Wire Wire Line
	7550 3650 7550 3900
Connection ~ 7800 3650
Connection ~ 7800 4650
Text HLabel 8050 3650 2    60   Input ~ 0
3V3
Text HLabel 8050 4650 2    60   Input ~ 0
GND
Text Notes 5700 5050 0    60   ~ 0
6.5mA on gate supply means 300nC at 20kHz leading to 71nF of Bootstrap C according to LM510xA datasheet.
$EndSCHEMATC
