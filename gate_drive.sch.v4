EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:power_bridge-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "Monster MK II"
Date "2016-11-02"
Rev "2"
Comp "ThunderDrive"
Comment1 "Gate Drive"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM5101x U1
U 1 1 5823D866
P 5600 3800
F 0 "U1" H 5600 4050 50  0000 C CNN
F 1 "LM5101x" H 5600 3450 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5600 4050 60  0001 C CNN
F 3 "" H 5600 4050 60  0000 C CNN
	1    5600 3800
	-1   0    0    -1  
$EndComp
Text HLabel 7750 3350 2    60   Input ~ 0
12V
Text HLabel 7700 4600 2    60   Input ~ 0
PWR_GND
Text HLabel 7700 3900 2    60   Output ~ 0
HIGHSIDE_GATE
Text HLabel 7700 4000 2    60   Input ~ 0
PHASE
Text HLabel 7700 4500 2    60   Output ~ 0
LOWSIDE_GATE
Text HLabel 4900 3900 0    60   Input ~ 0
LOWSIDE_PWM
Text HLabel 4900 4000 0    60   Input ~ 0
HIGHSIDE_PWM
$Comp
L C C5
U 1 1 5823E0A8
P 6150 4250
F 0 "C5" H 6175 4350 50  0000 L CNN
F 1 "4µ7" H 6175 4150 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 6188 4100 50  0001 C CNN
F 3 "" H 6150 4250 50  0000 C CNN
	1    6150 4250
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5823E2AB
P 6450 3700
F 0 "C4" H 6475 3800 50  0000 L CNN
F 1 "220n" H 6475 3600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6488 3550 50  0001 C CNN
F 3 "" H 6450 3700 50  0000 C CNN
	1    6450 3700
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5823E99F
P 7100 3900
F 0 "R2" V 7000 3850 50  0000 C CNN
F 1 "2R8" V 7100 3900 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603" V 7030 3900 50  0001 C CNN
F 3 "" H 7100 3900 50  0000 C CNN
	1    7100 3900
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5823EB91
P 7150 4500
F 0 "R3" V 7050 4450 50  0000 C CNN
F 1 "2R8" V 7150 4500 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603" V 7080 4500 50  0001 C CNN
F 3 "" H 7150 4500 50  0000 C CNN
	1    7150 4500
	0    1    1    0   
$EndComp
Text HLabel 7750 3450 2    60   Output ~ 0
GATE_SUPPLY
Wire Wire Line
	4900 3900 5200 3900
Wire Wire Line
	4900 4000 5200 4000
Wire Wire Line
	5000 4600 7700 4600
Wire Wire Line
	6250 3450 6250 3800
Wire Wire Line
	6150 4600 6150 4400
Connection ~ 6150 4600
Wire Wire Line
	7250 3900 7700 3900
Wire Wire Line
	7300 4500 7700 4500
Wire Wire Line
	6250 3450 7750 3450
Wire Wire Line
	6250 3800 6000 3800
Wire Wire Line
	6000 3900 6950 3900
Wire Wire Line
	5100 4500 7000 4500
Wire Wire Line
	5200 3700 5100 3700
Wire Wire Line
	5100 3700 5100 4500
Wire Wire Line
	5200 3800 5000 3800
Wire Wire Line
	5000 3800 5000 4600
$Comp
L D_Schottky D1
U 1 1 58322CAE
P 7200 3600
F 0 "D1" H 7200 3700 50  0000 C CNN
F 1 "NSR20F30NXT5G" H 7200 3500 50  0000 C CNN
F 2 "Diodes_SMD:D_0603" H 7200 3600 50  0001 C CNN
F 3 "" H 7200 3600 50  0000 C CNN
	1    7200 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3600 7550 3600
Wire Wire Line
	7550 3600 7550 3900
Connection ~ 7550 3900
Wire Wire Line
	7050 3600 6850 3600
Wire Wire Line
	6850 3600 6850 3900
Connection ~ 6850 3900
$Comp
L D_Schottky D2
U 1 1 58322FBC
P 7200 4200
F 0 "D2" H 7200 4300 50  0000 C CNN
F 1 "NSR20F30NXT5G" H 7200 4100 50  0000 C CNN
F 2 "Diodes_SMD:D_0603" H 7200 4200 50  0001 C CNN
F 3 "" H 7200 4200 50  0000 C CNN
	1    7200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4200 6850 4200
Wire Wire Line
	6850 4200 6850 4500
Connection ~ 6850 4500
Wire Wire Line
	7350 4200 7550 4200
Wire Wire Line
	7550 4200 7550 4500
Connection ~ 7550 4500
Wire Wire Line
	6450 3550 6450 3450
Connection ~ 6450 3450
Wire Wire Line
	6450 3850 6450 4000
Connection ~ 6450 4000
Wire Wire Line
	6000 4000 7700 4000
Wire Wire Line
	6000 3700 6150 3700
Wire Wire Line
	6150 3350 6150 4100
Wire Wire Line
	6150 3350 7750 3350
Connection ~ 6150 3700
Text Notes 6150 3250 0    60   ~ 0
6.5mA on gate supply means 300nC at 20kHz\nleading to 71nF of Bootstrap C \naccording to LM510xA datasheet.\n100n were added at the AMC1304 input.
$EndSCHEMATC
