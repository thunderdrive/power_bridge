EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:power_bridge-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Single phase"
Date "2017-03-17"
Rev "2"
Comp "ThunderDrive"
Comment1 "Monster MKII"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6150 2300 1250 1050
U 5836AA27
F0 "PowerStage" 60
F1 "power_phase.sch" 60
F2 "PWR_GND" I L 6150 3200 60 
F3 "LOWSIDE_GATE" I L 6150 2750 60 
F4 "PHASE" O L 6150 2650 60 
F5 "HIGHSIDE_GATE" I L 6150 2550 60 
F6 "PHASE_OUT" O R 7400 2650 60 
F7 "BAT+" I L 6150 2400 60 
$EndSheet
$Sheet
S 3750 2300 1650 650 
U 5836AA54
F0 "GateDrive" 60
F1 "gate_drive.sch" 60
F2 "12V" I L 3750 2400 60 
F3 "HIGHSIDE_GATE" O R 5400 2550 60 
F4 "PHASE" I R 5400 2650 60 
F5 "LOWSIDE_GATE" O R 5400 2750 60 
F6 "LOWSIDE_PWM" I L 3750 2700 60 
F7 "HIGHSIDE_PWM" I L 3750 2600 60 
F8 "GATE_SUPPLY" O R 5400 2400 60 
F9 "PWR_GND" I L 3750 2850 60 
$EndSheet
$Sheet
S 8050 2400 1200 1300
U 5836AAB5
F0 "CurrentSensor" 60
F1 "current_sensor.sch" 60
F2 "PHASE_OUT" O R 9250 2650 60 
F3 "GATE_DRIVE_SUPPLY" I L 8050 2500 60 
F4 "PHASE_IN" I L 8050 2650 60 
F5 "DATA" O R 9250 3100 60 
F6 "CLOCK" I R 9250 3000 60 
F7 "3V3" I R 9250 2850 60 
F8 "GND" I L 8050 3550 60 
$EndSheet
Wire Wire Line
	7400 2650 8050 2650
Wire Wire Line
	5400 2550 6150 2550
Wire Wire Line
	5400 2650 6150 2650
Wire Wire Line
	5400 2750 6150 2750
Wire Wire Line
	5400 2400 5650 2400
Wire Wire Line
	5650 2400 5650 2100
Wire Wire Line
	5650 2100 7750 2100
Wire Wire Line
	7750 2100 7750 2500
Wire Wire Line
	7750 2500 8050 2500
Wire Wire Line
	9250 2650 10350 2650
Wire Wire Line
	5850 2400 6150 2400
Wire Wire Line
	9250 2850 10050 2850
Wire Wire Line
	10050 2850 10050 4450
Wire Wire Line
	9950 4650 9950 3000
Wire Wire Line
	9950 3000 9250 3000
Wire Wire Line
	9250 3100 9850 3100
Wire Wire Line
	9850 3100 9850 4850
Wire Wire Line
	3400 2400 3400 2150
Wire Wire Line
	2800 2400 3750 2400
Wire Wire Line
	2600 2600 3750 2600
Wire Wire Line
	2700 2700 3750 2700
$Comp
L R R1
U 1 1 583819F6
P 8850 5250
F 0 "R1" V 8930 5250 50  0000 C CNN
F 1 "NTC 10k" V 8750 5250 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 8780 5250 50  0001 C CNN
F 3 "" H 8850 5250 50  0000 C CNN
	1    8850 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	9300 5050 9300 5250
Wire Wire Line
	9300 5250 9000 5250
Wire Wire Line
	8550 5250 8700 5250
Wire Wire Line
	8550 5150 8550 5250
Text Notes 5750 6200 0    60   ~ 0
The NC pins are Shield and must be connected to GND on the other side,\nTo asure there is no current flowing on these lines!
$Comp
L PWR_FLAG #FLG05
U 1 1 58398D91
P 3400 2150
F 0 "#FLG05" H 3400 2245 50  0001 C CNN
F 1 "PWR_FLAG" H 3400 2330 50  0000 C CNN
F 2 "" H 3400 2150 50  0000 C CNN
F 3 "" H 3400 2150 50  0000 C CNN
	1    3400 2150
	1    0    0    -1  
$EndComp
Connection ~ 3400 2400
$Comp
L PWR_FLAG #FLG06
U 1 1 58399129
P 9850 2200
F 0 "#FLG06" H 9850 2295 50  0001 C CNN
F 1 "PWR_FLAG" H 9850 2380 50  0000 C CNN
F 2 "" H 9850 2200 50  0000 C CNN
F 3 "" H 9850 2200 50  0000 C CNN
	1    9850 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2200 9850 2850
Connection ~ 9850 2850
Text HLabel 2100 3200 0    60   Input ~ 0
BAT-
Text HLabel 2100 1800 0    60   Input ~ 0
BAT+
Wire Wire Line
	5850 1800 5850 2400
Text HLabel 10350 2650 2    60   Output ~ 0
PHASE
Text Label 3150 2400 0    60   ~ 0
12V
Text Label 3150 2600 0    60   ~ 0
HS_PWM
Text Label 3150 2700 0    60   ~ 0
LS_PWM
$Comp
L GNDPWR #PWR07
U 1 1 58CCF80A
P 2400 3350
F 0 "#PWR07" H 2400 3150 50  0001 C CNN
F 1 "GNDPWR" H 2400 3220 50  0000 C CNN
F 2 "" H 2400 3300 50  0001 C CNN
F 3 "" H 2400 3300 50  0001 C CNN
	1    2400 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3200 6150 3200
Wire Wire Line
	2400 2850 2400 3350
Connection ~ 2400 3200
Connection ~ 2900 1800
Wire Wire Line
	2100 1800 5850 1800
Wire Wire Line
	5850 3550 8050 3550
Connection ~ 5850 3200
Text Notes 6000 3650 0    60   ~ 0
Route seperatly directly from BAT-\nno power current shall flow on this line.
Wire Wire Line
	10050 4450 7000 4450
Text Label 7250 4450 0    60   ~ 0
3V3
Text Label 7250 4650 0    60   ~ 0
CLOCK
Text Label 7250 4850 0    60   ~ 0
DATA
Text Label 7250 5150 0    60   ~ 0
TEMP+
Text Label 7250 5050 0    60   ~ 0
TEMP-
Text Label 7250 5250 0    60   ~ 0
HS_PWM
Text Label 7250 5350 0    60   ~ 0
LS_PWM
Wire Wire Line
	7750 4250 7750 5250
Wire Wire Line
	7750 4250 2600 4250
Wire Wire Line
	2600 4250 2600 2600
Wire Wire Line
	2700 2700 2700 4150
Wire Wire Line
	2700 4150 7850 4150
Wire Wire Line
	7850 4150 7850 5350
Wire Wire Line
	7950 4050 7950 5450
Wire Wire Line
	7950 4050 2800 4050
Wire Wire Line
	2800 4050 2800 2400
Text Label 7250 5450 0    60   ~ 0
12V
Wire Wire Line
	8150 5650 8150 3850
Wire Wire Line
	8150 3850 2900 3850
Text Label 7250 5650 0    60   ~ 0
VDC
Text Notes 3850 3950 0    60   ~ 0
Route seperatly directly from BAT-\nno power current shall flow on this line.
Connection ~ 5850 3550
NoConn ~ 6500 4450
NoConn ~ 6500 4550
NoConn ~ 6500 4650
NoConn ~ 6500 4750
NoConn ~ 6500 4850
NoConn ~ 6500 4950
NoConn ~ 6500 5050
NoConn ~ 6500 5250
NoConn ~ 6500 5350
Text Label 7250 5550 0    60   ~ 0
GND
Wire Wire Line
	7000 5650 8150 5650
Wire Wire Line
	5850 3200 5850 3950
Wire Wire Line
	5850 3950 8050 3950
Wire Wire Line
	8050 5550 7000 5550
Wire Wire Line
	7950 5450 7000 5450
Wire Wire Line
	7850 5350 7000 5350
Wire Wire Line
	7750 5250 7000 5250
Wire Wire Line
	7000 5150 8550 5150
Wire Wire Line
	7000 5050 9300 5050
NoConn ~ 7000 4950
NoConn ~ 7000 4750
NoConn ~ 7000 4550
Wire Wire Line
	7000 4650 9950 4650
Wire Wire Line
	9850 4850 7000 4850
NoConn ~ 6500 5150
NoConn ~ 6500 5450
$Comp
L har-flex_THR_26 J1
U 1 1 58CD969D
P 6750 5050
F 0 "J1" H 6750 5750 50  0000 C CNN
F 1 "har-flex_THR_26" V 6750 5050 50  0000 C CNN
F 2 "power_bridge:har-flex-THR-26pin-male-right" H 6750 3900 50  0001 C CNN
F 3 "" H 6750 3900 50  0001 C CNN
	1    6750 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3850 2900 1800
Wire Wire Line
	2400 2850 3750 2850
Wire Wire Line
	8050 3950 8050 5550
NoConn ~ 6500 5550
NoConn ~ 6500 5650
$EndSCHEMATC
