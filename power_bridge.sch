EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:power_bridge-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Single phase board"
Date "2016-11-02"
Rev "2"
Comp "ThunderDrive"
Comment1 "Monster MK II "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4600 2050 1900 550 
U 583610BA
F0 "Phase" 60
F1 "Phase.sch" 60
F2 "BAT-" I L 4600 2500 60 
F3 "BAT+" I L 4600 2150 60 
F4 "PHASE" O R 6500 2250 60 
$EndSheet
$Comp
L PWR_FLAG #FLG01
U 1 1 583A6658
P 4250 1850
F 0 "#FLG01" H 4250 1945 50  0001 C CNN
F 1 "PWR_FLAG" H 4250 2030 50  0000 C CNN
F 2 "" H 4250 1850 50  0000 C CNN
F 3 "" H 4250 1850 50  0000 C CNN
	1    4250 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2500 4250 2500
Connection ~ 4250 2500
$Comp
L +BATT #PWR02
U 1 1 583A740B
P 3800 1800
F 0 "#PWR02" H 3800 1650 50  0001 C CNN
F 1 "+BATT" H 3800 1940 50  0000 C CNN
F 2 "" H 3800 1800 50  0000 C CNN
F 3 "" H 3800 1800 50  0000 C CNN
	1    3800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2150 3800 2150
Connection ~ 3800 2150
Wire Wire Line
	4250 1850 4250 2750
Wire Wire Line
	3800 1800 3800 2750
$Comp
L PWR_FLAG #FLG03
U 1 1 58CC175A
P 3800 2750
F 0 "#FLG03" H 3800 2845 50  0001 C CNN
F 1 "PWR_FLAG" H 3800 2930 50  0000 C CNN
F 2 "" H 3800 2750 50  0000 C CNN
F 3 "" H 3800 2750 50  0000 C CNN
	1    3800 2750
	-1   0    0    1   
$EndComp
$Comp
L GNDPWR #PWR04
U 1 1 58CC178E
P 4250 2750
F 0 "#PWR04" H 4250 2550 50  0001 C CNN
F 1 "GNDPWR" H 4250 2620 50  0000 C CNN
F 2 "" H 4250 2700 50  0001 C CNN
F 3 "" H 4250 2700 50  0001 C CNN
	1    4250 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
